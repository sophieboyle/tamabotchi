Tamabotchi, a Tamagotchi Discord Bot running on Python!
=======================================================

# Bot Setup

Clone the repository:

`git clone https://gitlab.com/sophieboyle/tamabotchi.git`

Download requirements:

`cd tamabotchi`

`pip3 install -r requirements.txt`

Initialise a config.json according to the example config.json.

# Database Setup

Install postgresql:

https://www.postgresql.org/download/

Restore database:

`pg_restore tamabotchidb.dump`

Run database:

`sudo -u postgres psql -d tamabotchidb`

# Running the Bot

Run on python using:

`python3 src/main.py`