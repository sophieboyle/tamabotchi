import asyncio
import asyncpg
import random


async def id_in_db(db, usr_id):
    """Checks if an ID is present in the datbase. Inserts it if none.
    
    Arguments:
        db {database} -- The database file.
        usr_id {int} -- User's discord ID.
    """
    registered_id = await db.fetchrow('SELECT * FROM users WHERE id = $1', usr_id)
    if registered_id == None:
        await db.execute('''INSERT INTO users(id, money) VALUES($1, $2)''', usr_id, 0)


async def initialise_inventory(db, usr_id):
    """Initialises a user's inventory.
    
    Arguments:
        db {database} -- Database
        usr_id {int} -- User's discord ID.
    """
    registered_id = await db.fetchrow('SELECT * FROM inventory WHERE id = $1', usr_id)
    if registered_id == None:
        await db.execute('INSERT INTO inventory(id, meat, bread, carrot, icecream, cake) \
                            VALUES($1, $2, $3, $4, $5, $6)',usr_id, 0, 0, 0, 0, 0)


async def get_tamagotchi(db, usr_id):
    """If the user has a tamagotchi, gets its state.
    If the user doesn't have a tamagotchi, returns None.
    
    Arguments:
        db {database} -- Database.
        usr_id {int} -- User's discord ID.
    
    Returns:
        string -- Tamagotchi name.
        string -- Tamagotchi's state.
    """
    tamagotchi = await db.fetchval('SELECT tamagotchi FROM tamagotchi WHERE id = $1', usr_id)
    state = await db.fetchval('SELECT state FROM tamagotchi WHERE id = $1', usr_id)
    return tamagotchi, state


async def set_state(db, usr_id, new_state):
    """Sets the state of the user's tamagotchi.
    
    Arguments:
        db {database} -- Database.
        usr_id {int} -- User's discord ID.
        new_state {string} -- Tamagotchi's new state to be set.
    """
    await db.execute('''UPDATE tamagotchi SET state = $1 WHERE id = $2''', new_state, usr_id)


async def set_tamagotchi(db, usr_id, tamagotchi, gender):
    """Sets a new tamagotchi for the user.
    
    Arguments:
        db {database} -- Database
        usr_id {int} -- User's discord ID.
        tamagotchi {string} -- Tamagotchi to be set.
    """
    await db.execute('''INSERT INTO tamagotchi(id, tamagotchi, exp, gender, care) VALUES($1, $2, $3, $4, $5)''', usr_id, tamagotchi, 0.0, gender, 1.0)
    await set_state(db=db, usr_id=usr_id, new_state="idle")
    for stat in ["happiness", "bladder", "energy", "fullness"]:
        await set_stats(db=db, usr_id=usr_id, amount=1.0, stat=stat)


async def set_stats(db, usr_id, amount, stat):
    """Set a stat to a given amount.
    
    Arguments:
        db {database} -- Database.
        usr_id {int} -- User's discord ID.
        amount {float} -- Decimal number to set the stat to.
        mood {string} -- Stat to set.
    """
    await db.execute('''UPDATE tamagotchi SET {} = $1 WHERE id = $2'''.format(stat), amount, usr_id)


async def get_stat(db, usr_id, stat):
    """Fetches the stat for a given user's tamagotchi.
    
    Arguments:
        db {database} -- Database.
        usr_id {int} -- User's discord ID.
        stat {string} -- Stat to fetch.
    
    Returns:
        float -- Value of the given stat.
    """
    return await db.fetchval('SELECT {} FROM tamagotchi WHERE id = $1'.format(stat), usr_id)


async def drain(db):
    """Drains happiness, bladder, and energy for all tamagotchi in the database, every 3 hours.
    
    Arguments:
        db {database} -- Database
    """
    while True:
        await db.execute('''UPDATE tamagotchi SET happiness = happiness-0.15, bladder = bladder-0.10, energy=energy-0.12''')
        await asyncio.sleep(60*60*3)


async def drain_individual_stat(db, usr_id, stat, amount):
    """Drains a given amount of energy from a given user's tamagotchi.
    
    Arguments:
        db {database} -- Database
        usr_id {int} -- User's discord ID.
        amount {float} -- Amount to be drained.
    """
    await db.execute("UPDATE tamagotchi SET {0} = {0}-$1 WHERE id=$2".format(stat), amount, usr_id)


async def check_state(happiness, bladder, energy, fullness):
    """Calculates the tamagotchi's new state.
    
    Arguments:
        happiness {float} -- Float describing tamagotchi's happiness stat.
        bladder {float} -- Float describing tamagotchi's bladder stat.
        energy {float} -- Float describing tamagotchi's energy stat.
    """
    if (happiness <= 0.0) or (bladder <= 0.0) or (energy <= 0.0):
        return "dead"
    elif (happiness < 0.45):
        return "sad"
    elif (bladder < 0.45):
        return "angry"
    elif (fullness < 0.45):
        return "hungry"
    elif (energy <0.45):
        return "sleepy"
    elif (happiness >= 0.7) and (bladder >= 0.7) and (energy >= 0.7):
        return "happy"
    else:
        return "neutral"


async def update_all_states(db):
    """Updates the state and care of each user's tamagotchi every three hours.
    
    Arguments:
        db {database} -- Database.
    """
    while True:
        users = await db.fetch('SELECT id FROM tamagotchi')
        if users:
            for user in list(users):
                await update_individual_state(db=db, usr_id=user["id"])
                await update_individual_care(db=db, usr_id=user["id"])
        await asyncio.sleep(60*60*3)


async def update_individual_state(db, usr_id):
    """Updates the state of a given user's tamagotchi.
    
    Arguments:
        db {database} -- Database
        usr_id {int} -- User's discord id
    """
    tamagotchi, old_state = await get_tamagotchi(db=db, usr_id=usr_id)
    if old_state != "dead":
        stats = {}
        for stat in ["happiness", "bladder", "energy", "fullness"]:
            stats[stat] = await get_stat(db=db, usr_id=usr_id, stat=stat)
        new_state = await check_state(happiness=stats["happiness"], bladder=stats["bladder"], energy=stats["energy"], fullness=stats["fullness"])
        await set_state(db=db, usr_id=usr_id, new_state=new_state)


async def update_individual_care(db, usr_id):
    """Updates a given user's tamagotchi's care level based on what state the tamagotchi is in.
    Only does this if the current care level is between 0 and 1.
    
    Arguments:
        db {database} -- Database.
        usr_id {int} -- User's discord ID.
    """
    current_care = await get_care(db=db, usr_id=usr_id)
    if (current_care > 0.0) and (current_care < 1.0):
        tamagotchi, state = await get_tamagotchi(db=db, usr_id=usr_id)
        if (state in ["sad", "angry", "sleepy", "hungry"]):
            await db.execute('UPDATE tamagotchi SET care = care - $1 WHERE id = $2', 0.1, usr_id)
        elif (state in ["happy", "sleep"]):
            await db.execute('UPDATE tamagotchi SET care = care + $1 WHERE id = $2', 0.1, usr_id)
        else:
            return


async def get_care(db, usr_id):
    """Returns a given user's tamagotchi's care level.
    
    Arguments:
        db {database} -- Database.
        usr_id {int} -- User's discord id.
    
    Returns:
        float -- Care level.
    """
    care = await db.fetchval('SELECT care FROM tamagotchi WHERE id = $1', usr_id)
    return care


async def calculate_care(care):
    """Returns whether the care level is good or bad.
    
    Arguments:
        care {float} -- Care level.
    
    Returns:
        string -- "good" or "bad".
    """
    if care >= 0.5:
        return "good"
    else:
        return "bad"


async def add_exp(db, usr_id, exp):
    """Adds a given amount of experience to a user's tamagotchi.
    Checks if the tamagotchi has evolved or not.
    
    Arguments:
        db {database} -- Database
        usr_id {int} -- User's discord id
        exp {float} -- Exp to add

    Returns:
        boolean -- True if evolved, False otherwise
    """
    await db.execute('''UPDATE tamagotchi SET exp = exp+$1 WHERE id = $2''', exp, usr_id)
    return await check_growth(db=db, usr_id=usr_id)


async def check_growth(db, usr_id):
    """Checks the tamagotchi's experience, and if it has reached a multiple of 2.0, changes the tamagotchi to its next evolution.
    The evolution is picked based on the evolutions dictionary, which includes family tree, care levels, gender, and care.
    
    Arguments:
        db {database} -- Database

    Returns:
        boolean -- True if evolved, False otherwise
    """
    evolutions = {"marutchi": {"evolves from": ["babytchi", "shirobabytchi"], "gender":["male", "female"], "care": ["good", "bad"]},
                    "tamatchi": {"evolves from": ["marutchi"], "gender": ["male", "female"], "care": ["good"]},
                    "kuchitamatchi": {"evolves from": ["marutchi"], "gender": ["male", "female"], "care": ["bad"]},
                    "mametchi": {"evolves from": ["tamatchi"], "gender":["male"], "care":["good", "bad"]},
                    "mimitchi": {"evolves from": ["tamatchi"], "gender":["female"], "care":["good", "bad"]},
                    "hashizotchi": {"evolves from": ["kuchitamatchi"], "gender":["male"], "care":["good", "bad"]},
                    "tarakotchi": {"evolves from": ["kuchitamatchi"], "gender":["female"], "care":["good", "bad"]}}
    
    care_level = await db.fetchval('SELECT care FROM tamagotchi WHERE id = $1', usr_id)
    care = await calculate_care(care=care_level)

    if (float(await db.fetchval('''SELECT exp FROM tamagotchi WHERE id = $1''', usr_id))%2==0.0):
        current_tamagotchi = await db.fetchval('''SELECT tamagotchi FROM tamagotchi WHERE id = $1''', usr_id)
        gender = await db.fetchval('''SELECT gender FROM tamagotchi WHERE id = $1''', usr_id)

        for evolution in evolutions.keys():
            if (current_tamagotchi in evolutions[evolution]["evolves from"]) and \
                (gender in evolutions[evolution]["gender"]) and \
                    (care in evolutions[evolution]["care"]):
                await change_tamagotchi(db=db, usr_id=usr_id, new_tamagotchi=evolution) 
                return True

    else:
        return False


async def remove_tamagotchi(db, usr_id):
    """Removes user's tamagotchi.
    
    Arguments:
        db {database} -- Database.
        usr_id {int} -- User's discord id.
    """
    await db.execute('''DELETE FROM tamagotchi WHERE id = $1''', usr_id)


async def change_tamagotchi(db, usr_id, new_tamagotchi):
    """Change user's tamagotchi.
    
    Arguments:
        db {database} -- Database.
        usr_id {int} -- User's discord id.
        new_tamagotchi {string} -- New tamagotchi to set to.
    """
    await db.execute('''UPDATE tamagotchi SET tamagotchi = $1 WHERE id = $2''', new_tamagotchi, usr_id)


async def get_money(db, usr_id):
    """Gets a given user's balance.
    
    Arguments:
        db {database} -- Database.
        usr_id {int} -- User's discord id.
    
    Returns:
        int -- User's balance.
    """
    return await db.fetchval('''SELECT money FROM users WHERE id = $1''', usr_id)


async def deposit_money(db, usr_id, amount):
    """Adds money to given user's account.
    
    Arguments:
        db {database} -- Database.
        usr_id {int} -- User's discord is.
        amount {int} -- Money to add.
    """
    await db.execute('''UPDATE users SET money = money + $1 WHERE id = $2''', amount, usr_id)


async def withdraw_money(db, usr_id, amount):
    """Subtracts money from a given user's account if they have enough.
    
    Arguments:
        db {database} -- Database.
        usr_id {int} -- User's discord id.
        amount {int} -- Money to subtract.

    Returns:
        boolean -- True if withdrawal successful, False otherwise.
    """
    balance = await db.fetchval('''SELECT money FROM users WHERE id = $1''', usr_id)
    if balance >= amount:
        await db.execute('''UPDATE users SET money = money - $1 WHERE id = $2''', amount, usr_id)
        return True
    return False


async def add_to_inventory(db, usr_id, item):
    """Adds a given item to a given user's inventory.
    
    Arguments:
        db {database} -- Database.
        usr_id {int} -- User's discord ID.
        item {string} -- Item to add.
    """
    await db.execute('UPDATE inventory SET {0} = {0} + 1 WHERE id = $1'.format(item), usr_id)


async def remove_from_inventory(db, usr_id, item):
    """Attempts to remove a given item from a given user's inventory.
    Does not remove if user has 0 instances of the given item.
    
    Arguments:
        db {database} -- Database.
        usr_id {int} -- User's discord ID.
        item {string} -- Item to be removed
    
    Returns:
        bool -- True if removal is successful, False otherwise.
    """
    amount = await db.fetchval('SELECT {} FROM inventory WHERE id = $1'.format(item), usr_id)
    if amount !=0:
        await db.execute('UPDATE inventory SET {0} = {0} - 1 WHERE id = $1'.format(item), usr_id)
        return True
    else:
        return False