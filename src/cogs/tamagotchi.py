import discord
from discord.ext import commands
import asyncio
import os
from cogs.database import get_tamagotchi, set_tamagotchi, drain, get_stat, set_stats, set_state, remove_tamagotchi, add_exp, check_growth, update_individual_state, remove_from_inventory, drain_individual_stat
from discord import File
import time
import random
from cogs.imageproc import stitch
from functools import partial

# TODO: Implement feed tamagotchi.


async def no_tamagotchi(ctx):
    """Bot statement if the tamagotchi does not exist.
    
    Arguments:
        ctx {discord.ext.commands.ctx} -- Message context.
    """
    await ctx.channel.send("{} you do not have a tamagotchi yet! Choose one from mametchi or hashizotchi, with t.tamagotchi choose $name.".format(ctx.author.mention))


async def dead_tamagotchi(self, ctx, tamagotchi, state):
    """Bot statement if the tamagotchi is dead.
    
    Arguments:
        ctx {discord.ext.commands.ctx} -- Message context.
    """
    await ctx.channel.send("{} your tamagotchi is dead.".format(ctx.author.mention))
    await view_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state=state)


async def sleeping_tamagotchi(self, ctx, tamagotchi, state):
    """Bot statement if the tamagotchi is sleeping.
    
    Arguments:
        ctx {discord.ext.commands.ctx} -- Message context.
    """
    await ctx.channel.send("{} your tamagotchi is sleeping!".format(ctx.author.mention))
    await view_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state=state)


async def view_tamagotchi(self, ctx, tamagotchi, state, food=None):
    """Views tamagotchi if it exists.
    If not, reminds the user.
    
    Arguments:
        ctx {discord.ext.commands.context} -- Message context.
    """
    if tamagotchi != None:
        if food == None:
            image_b = await stitch(options={"wallpapers":"spotty", 
                             "tamagotchi": f"{tamagotchi}/{state}"})
        else:
            image_b = await stitch(options={"wallpapers":"spotty", 
                             "tamagotchi": f"{tamagotchi}/{state}",
                             "food":food}) 
        await ctx.channel.send(file=discord.File
                                (fp=image_b, filename="tamagotchi.png"))
    else:
        await no_tamagotchi(ctx)


async def tamagotchi_grow(self, ctx, exp):
    """Adds a given amount of experience to user's tamagotchi.
    Bot also mentions if the tamagotchi has evolved or not.
    
    Arguments:
        ctx {discord.ext.commands.ctx} -- Message context
        exp {float} -- Experience to be added
    Returns:
        [boolean] -- True if evolved, False otherwise
    """
    evolved = await add_exp(db=self.bot.db, usr_id=ctx.author.id, exp=exp)
    tamagotchi, state = await get_tamagotchi(db=self.bot.db, usr_id=ctx.author.id)
    if evolved:
        await ctx.channel.send("{} congratulations, your tamagotchi has evolved into {}!".format(ctx.author.mention, tamagotchi))
        await view_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state=state)
        return True
    else:
        return False


class Tamagotchi(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    
    @commands.group(invoke_without_command=True)
    async def tamagotchi(self, ctx):
        """Sets up tamagotchi cog and sends a description of all of the commands to the chat.
        
        Arguments:
            ctx {discord.ext.commands.context} -- Message context.
        """
        await ctx.channel.send("Tamagotchi commands are as follows:\n\
                                t.tamagotchi new -- Get a new tamagotchi\n\
                                t.tamagotchi view -- View your tamagotchi\n\
                                t.tamagotchi play -- Play with your tamagotchi to increase happiness\n\
                                t.tamagotchi toilet -- Let your tamagotchi go to the bathroom\n\
                                t.tamagotchi sleep -- Put your tamagotchi to sleep to increase energy\n\
                                t.tamagotchi abandon -- Abandon your tamagotchi")


    @tamagotchi.command(pass_context=True)
    async def view(self, ctx):
        """Views the tamagotchi using the view function.
        
        Arguments:
            ctx {discord.ext.commands.ctx} -- Message context.
        """
        await update_individual_state(db=self.bot.db, usr_id=ctx.author.id)
        tamagotchi, state = await get_tamagotchi(db=self.bot.db, usr_id=ctx.author.id)
        await view_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state=state)


    @tamagotchi.command(pass_context=True)
    async def new(self, ctx):
        """Allows user to recieve a new tamagotchi.
        If the user already has a tamagotchi, the bot reminds them.
        
        Arguments:
            ctx {discord.ext.commands.context} -- Message context
        """
        tamagotchi = random.choice([("babytchi", "male"), ("shirobabytchi", "female")])
        if (await get_tamagotchi(db=self.bot.db, usr_id=ctx.author.id) == (None, None)):
            await set_tamagotchi(db=self.bot.db, usr_id=ctx.author.id, tamagotchi=tamagotchi[0], gender=tamagotchi[1])
            await ctx.channel.send("{} you have recieved {}! View your new tamagotchi using t.tamagotchi view.".format(ctx.author.mention, tamagotchi[0]))
        else:
            await ctx.channel.send("{} you already have a tamagotchi!".format(ctx.author.mention))


    @tamagotchi.command(pass_context=True)
    async def play(self, ctx):
        """Plays with tamagotchi to increase happiness stat.
        If the tamagotchi is dead, or the user does not have a tamagotchi, the bot will remind the user.
        Also adds experience and checks if it has evolved.
        
        Arguments:
            ctx {discord.ext.commands.context} -- Message context.
        """
        tamagotchi, throwaway_state = await get_tamagotchi(db=self.bot.db, usr_id=ctx.author.id)
        if (tamagotchi!=None) and (throwaway_state!="dead") and (throwaway_state!="sleep"):
            await set_stats(db=self.bot.db, usr_id=ctx.author.id, amount=1.0, stat="happiness")
            await drain_individual_stat(db=self.bot.db, usr_id=ctx.author.id, stat="energy", amount=0.1)
            if not await tamagotchi_grow(self=self, ctx=ctx, exp=0.2):
                await view_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state="crazy")
        elif throwaway_state == "dead":
            await dead_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state=throwaway_state)
        elif throwaway_state == "sleep":
            await sleeping_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state=throwaway_state)
        else:
            await no_tamagotchi(ctx=ctx)
    

    @tamagotchi.command(pass_context=True)
    async def toilet(self, ctx):
        """Allows Tamagotchi to go to the bathroom to increase bladder stat.
        If the tamagotchi is dead, or the user does not have a tamagotchi, the bot will remind the user.
        Also adds experience and checks if it has evolved.
        
        Arguments:
            ctx {discord.ext.commands.context} -- Message context
        """
        tamagotchi, throwaway_state = await get_tamagotchi(db=self.bot.db, usr_id=ctx.author.id)
        if (tamagotchi!=None) and (throwaway_state!="dead"):
            await set_stats(db=self.bot.db, usr_id=ctx.author.id, amount=1.0, stat="bladder")
            await drain_individual_stat(db=self.bot.db, usr_id=ctx.author.id, stat="energy", amount=0.1)
            if not await tamagotchi_grow(self=self, ctx=ctx, exp=0.2):
                await view_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state="toilet")
        elif throwaway_state == "dead":
            await dead_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state=throwaway_state)
        elif throwaway_state == "sleep":
            await sleeping_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state=throwaway_state)
        else:
            await no_tamagotchi(ctx=ctx)
    

    @tamagotchi.command(pass_context=True)
    async def sleep(self, ctx):
        """Puts Tamagotchi to sleep to increase sleep stat.
        If the tamagotchi is dead, or the user does not have a tamagotchi, the bot will remind the user.
        Also adds experience and checks if it has evolved.
        
        Arguments:
            ctx {discord.ext.commands.ctx} -- Message context.
        """
        tamagotchi, state = await get_tamagotchi(db=self.bot.db, usr_id=ctx.author.id)
        if (tamagotchi!=None) and (state !="dead"):
            await set_stats(db=self.bot.db, usr_id=ctx.author.id, amount=1.0, stat="energy")
            await set_state(db=self.bot.db, usr_id=ctx.author.id, new_state="sleep")
            if not await tamagotchi_grow(self=self, ctx=ctx, exp=0.2):
                await view_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state="sleep")
        elif state == "dead":
            await dead_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state=state)
        else:
            await no_tamagotchi(ctx=ctx)
    

    @tamagotchi.command(pass_context=True)
    async def feed(self, ctx, food):
        """Feeds a user's tamagotchi a given food if in the user's inventory.
        If not, bot will remind the user.
        
        Arguments:
            ctx {discord.ext.commands.ctx} -- Message context.
            food {string} -- Food to feed.
        """
        tamagotchi, state = await get_tamagotchi(db=self.bot.db, usr_id=ctx.author.id)
        if (tamagotchi!=None) and (state!="dead"):
            success = await remove_from_inventory(db=self.bot.db, usr_id=ctx.author.id, item=food)
            if success:
                await ctx.channel.send("{} you have fed your tamagotchi {}".format(ctx.author.mention, food))
                await set_stats(db=self.bot.db, usr_id=ctx.author.id, amount=1.0, stat="fullness")
                await view_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state="full", food=food)
            else:
                await ctx.channel.send("{} you do not have enough {} to feed your tamagotchi. Buy some more at the shop using t.shop!".format(ctx.author.mention, food))
                await view_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state="hungry")
        elif (state == "dead"):
            await dead_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state=state)
        else:
            await no_tamagotchi(ctx=ctx)

    
    @tamagotchi.command(pass_context=True)
    async def abandon(self, ctx):
        """Abandons tamagotchi to remove it from the database.
        Additionally, sends a crying sprite and guilt-trips the user.
        
        Arguments:
            ctx {discord.ext.commands.context} -- Message context
        """
        tamagotchi, state = await get_tamagotchi(db=self.bot.db, usr_id=ctx.author.id)
        await ctx.channel.send("{}, say goodbye to your tamagotchi forever.".format(ctx.author.mention))
        if (state == "dead"):
            await view_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state=state)
        else:
            await view_tamagotchi(self=self, ctx=ctx, tamagotchi=tamagotchi, state="cry")
        await remove_tamagotchi(db=self.bot.db, usr_id=ctx.author.id)


def setup(bot):
    bot.add_cog(Tamagotchi(bot))
