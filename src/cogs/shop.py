import asyncio
from cogs.database import deposit_money, withdraw_money, get_money, add_to_inventory
from cogs.exceptions import InvalidOperation
import logging
from discord.ext import commands
from discord import File
import os


async def alter_money(self, ctx, transaction, amount):
    """Alters the money in a given user's account.
    Can either deposit or withdraw based on specified transaction.
    
    Arguments:
        ctx {discord.ext.commands.ctx} -- Message context.
        usr_id {int} -- User's discord id.
        transaction {string} -- Operation: either withdrawal or deposit.
        amount {int} -- Amount of money to be withdrawn/deposited.
    Returns:
        bool -- True if transaction was successful, false otherwise.
    """
    if transaction == "deposit":
        await deposit_money(db=self.bot.db, usr_id=ctx.author.id, amount=amount)
        return True
    elif transaction == "withdraw":
        successful_withdrawal = await withdraw_money(db=self.bot.db, usr_id=ctx.author.id, amount=amount)
        if not successful_withdrawal:
            await ctx.channel.send("{}, you do not have enough money in your account for this action.".format(ctx.author.mention))
            return False
        return True
    else:
        logging.info("Invalid Operation in cogs.shop.alter_money(): was neither deposit or withdrawal.")


async def get_food_sprite(food):
    """Returns the food sprite image for a given food.
    TODO: Utilise the dictionary of {food:cost}, and raise exception if the food is not found in file-system.
    
    Arguments:
        food {string} -- Food to get the sprite for.
    
    Returns:
        string -- Path to food sprite.
    """
    for root, dirs, filenames in os.walk("sprites/food"):
        for filename in filenames:
            if filename == food+".png":
                return "sprites/food/"+filename


class Shop(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        # Initialise with shop's food items.
        self.food = {"bread":100, "carrot":200, "icecream":300, "meat":400, "cake":500}
    
    
    @commands.group(invoke_without_command=True)
    async def shop(self, ctx):
        """Sets up shop cog.
        
        Arguments:
            ctx {discord.ext.commands.ctx} -- Message context.
        """
        await ctx.channel.send("Shop commands are as follows:\n\
                                t.shop balance -- View your balance.\n\
                                t.shop menu -- View food and their prices.\n\
                                t.shop buy $food -- Purchase food.")


    @shop.command(pass_context=True)
    async def balance(self, ctx):
        """Allows a user to view their balance.
        
        Arguments:
            ctx {discord.ext.commands.ctx} -- Message context.
        """
        balance = await get_money(db=self.bot.db, usr_id=ctx.author.id)
        await ctx.channel.send("{}, your balance is ¥{}".format(ctx.author.mention, balance))
    
    
    @shop.command(pass_context=True)
    async def buy(self, ctx, food):
        """Allows user to buy food and displays the sprite of the purchased food.
        Withdraws a corresponding amount of money from their balance.
        If the food is not valid, bot reminds the user.
        
        Arguments:
            ctx {discord.ext.commands.ctx} -- Message context.
            food {string} -- Given food to buy.
        """
        sprite_path = await get_food_sprite(food=food.lower())
        if sprite_path:
            success = await alter_money(self=self, ctx=ctx, transaction="withdraw", amount=self.food[food.lower()])
            if success:
                await ctx.channel.send("{}, you have purchased a {}".format(ctx.author.mention, food), file=File(sprite_path))
                await add_to_inventory(db=self.bot.db, usr_id=ctx.author.id, item=food)
        else:
            await ctx.channel.send("{}, {} is not a valid food choice.".format(ctx.author.mention, food))
    

    @shop.command(pass_context=True)
    async def menu(self, ctx):
        """Displays food and prices.
        
        Arguments:
            ctx {discord.ext.commands.ctx} -- Message context.
        """
        menu = ""
        for food in self.food.keys():
            menu += "{} -- ¥{}\n".format(food, self.food[food])
        await ctx.channel.send(menu)


def setup(bot):
    bot.add_cog(Shop(bot))