class Error(Exception):
    """Base class for exceptions.
    
    Arguments:
        Exception {exception} -- Exception
    """
    pass


class InvalidOperation(Error):
    """Raised when operation is invalid
    
    Arguments:
        Error {Error} -- Error
    """
    pass