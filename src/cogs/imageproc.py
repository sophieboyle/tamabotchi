from PIL import Image
import asyncio
from io import BytesIO
import random

async def stitch(options):
    """Stitches together images of sprites
    
    Arguments:
        options {dict} -- Dictionary of sprite category mapped to sprite name
    
    Returns:
        BytesIO object -- Byte form of the stitched image
    """
    positions = {"tamagotchi": (random.randint(0, 130), random.randint(10, 16)), "wallpapers":(0,0), "food":(60, 25)}
    stitch = Image.new("RGBA", (160, 48), color=(0,0,0,0))

    for sprite in options.keys():
        image = Image.open(f"sprites/{sprite}/{options[sprite]}.png")

        if random.choice([True, False]) and sprite=="tamagotchi":
            image = image.transpose(Image.FLIP_LEFT_RIGHT)

        stitch.paste(image, positions[sprite], mask=image)

    file = BytesIO()
    stitch.save(file, format="png")
    file.seek(0)

    return file