import discord
import asyncio
import asyncpg
import json
from discord.ext import commands
from cogs.database import id_in_db, drain, update_all_states, deposit_money, initialise_inventory


bot = commands.Bot(command_prefix="t.")


@bot.event
async def on_ready():
    """
    Indicates that the bot is running
    """
    print("Tamabotchi is running.")


async def run():
    """Initialises bot with postgresql database.
    """

    with open("config.json", "r") as f:
        config = json.load(f)
        token = config["token"]
        dbpassword = config["dbpassword"]

    credentials = {"user":"postgres", "password":dbpassword, "database":"tamabotchidb", "host":"0.0.0.0"}

    db = await asyncpg.create_pool(**credentials)
    bot.db = db

    bot.loop.create_task(drain(db=bot.db))
    bot.loop.create_task(update_all_states(db=bot.db))

    try:
        await bot.start(token)
    except KeyboardInterrupt:
        await db.close()
        await bot.logout()


@bot.event
async def on_message(message):
    """Typical on message function.
    Ignores self.
    Proceeds to check message for command.
    
    Arguments:
        message {discord obj} -- Message input into discord chat
    """
    if message.author == bot.user:
        return

    # Register user in database if not already registered.
    await register(usr_id=message.author.id, db=bot.db)

    # Listen for commands.
    await bot.process_commands(message)

    # Add money to user's account.
    await deposit_money(db=bot.db, usr_id=message.author.id, amount=10)


async def register(usr_id, db):
    """Registers a user's ID in the database.
    
    Arguments:
        ctx {discord.ext.commands.context} -- Message context
    """
    await id_in_db(db=db, usr_id=usr_id)
    await initialise_inventory(db=db, usr_id=usr_id)


if __name__ == "__main__":
    # Add cogs
    cogs = ["cogs.tamagotchi", "cogs.shop"]
    for cog in cogs:
        bot.load_extension(cog)

    # Set event loops and run bot forever
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run())